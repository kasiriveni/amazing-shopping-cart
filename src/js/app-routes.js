'use strict';

angular.module('amazingShopingCart')
.config(['$routeProvider',function(routeProvider){
	routeProvider
	.when('/tryprime',{
		template:'<div>hello {{welcome}}</div>',
		controller:['$scope',function($scope){
			$scope.welcome ='Prime';
		}]
	})
	.when('/cart',{
		templateUrl: '/templates/cartTemplate.html',
		controller:['$scope', 'myFactory', function($scope, myFactory){
			$scope.welcome ='cart';
			myFactory.then(function(data){
				console.log('data', data);
				$scope.datas = data.data;
			});
		}]
	});

}]);