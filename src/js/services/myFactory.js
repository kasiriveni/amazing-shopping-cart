'use strict';

angular.module('amazingShopingCart')
.factory('myFactory', ['$http',function($http) {
  return $http.get('http://localhost:9000/students_items');
}]);
