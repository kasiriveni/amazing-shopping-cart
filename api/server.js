var express = require('express');
var app     = express();
const router = express.Router();

app.use(require('connect-livereload')({
    port: 9090
}));

app.use(express.static('.build'));

app.get('/',function(req,res){
  res.sendfile('./index.html');
});

app.get('/product_items', (req, res) => {
  res.sendfile('./api/data/data.json');
});

app.get('/students_items', (req, res) => {
  res.sendfile('./api/data/students.json');
});

app.listen(9000, function() {
    console.log("Listening on 9000");
});
