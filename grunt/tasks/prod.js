'use strict';

module.exports = function(grunt) {
    grunt.registerTask('prod', ['build', 'express:server']);
    grunt.registerTask('prod-dev', ['build:devbuild', 'express:server', 'watch']);
};
