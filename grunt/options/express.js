'use strict';

module.exports = {
    server: {
        options: {
            script: './api/server.js'
        }
    }
};